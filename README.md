This a group project on "Dots and Lines Game".
Here we will be using Pygame library in Python.


'Dots and Boxes' is a game played for fun. It is a game in which each player has to draw a line, by joining any two consecutive dots either horizontally or vertically, which have not already been joined. When a player's line drawn is the last line to form a square box (formed by 4 dots only), then that player's score gets incremented. This game continues until all the dots are joined. The player with the highest score is declared as the winner and the score is displayed. You can restart the game, to play a new game or simply quit. Similarly 'Dots and Triangles' game is played by forming triangles (formed by 3 dots only) instead of boxes.


Team -
T.Keerthi Sri,
R.Varshini Reddy.


Game Work Division -

Varshini: formatting Pygame window, initializing the dots, checking win condition, updating boxes won by the player, resetting boxes and score, updating next player's turn, adding medium level game, designing the menu.

Keerthi: Pygame running window, combining resetting (scores, players, and boxes) functions, box formation (keyboard and mouse controls), score incrementation, drawing lines, game over condition, adding dots and triangles game (hard level).

References -
https://www.pygame.org/docs/
https://github.com/pyGuru123/Python-Games/blob/master/Dots%20%26%20Boxes/main.py