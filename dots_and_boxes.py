import pygame
import sys

pygame.init()
pygame.font.init()

WIDTH = HEIGHT = 600
PADDING = 40
SIZE = 100
ROWS = COLS = (WIDTH - 4 * PADDING) // SIZE

font = pygame.font.SysFont('cursive', 32)
FONT = pygame.font.SysFont('cursive', 55)

screen = pygame.display.set_mode((WIDTH, HEIGHT))

pygame.display.set_caption("Dots and Boxes Game")

class Box:
    def __init__(self, row, col):
        self.row = row
        self.col = col
        self.index = self.row * ROWS + self.col
        self.box = pygame.Rect((self.col * SIZE + 2 * PADDING, self.row * SIZE + 3 * PADDING, SIZE, SIZE))
        self.left = self.box.left
        self.right = self.box.right
        self.top = self.box.top
        self.bottom = self.box.bottom

        self.edges = [
            [(self.left, self.top), (self.right, self.top)],
            [(self.right, self.top), (self.right, self.bottom)],
            [(self.right, self.bottom), (self.left, self.bottom)],
            [(self.left, self.bottom), (self.left, self.top)]
        ]
        self.sides = [False, False, False, False]
        self.winner = None

    def checkwin(self, winner):
        if not self.winner:
            if self.sides == [True] * 4:
                self.winner = winner
                self.colour = 'orange' if winner == 'X' else 'green'
                self.text = font.render(self.winner, True, 'white')
                return 1
        return 0

    def update(self, screen):
        if self.winner:
            pygame.draw.rect(screen, self.colour, self.box)
            screen.blit(self.text, (self.box.centerx - 5, self.box.centery - 7))
        for index, side in enumerate(self.sides):
            if side:
                pygame.draw.line(screen, 'black', self.edges[index][0], self.edges[index][1], 3) 

def create_boxes() -> list:
    boxes = []
    for row in range(ROWS):
        for col in range(COLS):
            box = Box(row, col)
            boxes.append(box)
    return boxes

def reset_players():
    chance = 0
    players = ['X', 'O']
    player = players[chance]
    next_chance = False
    return chance, players, player, next_chance

def reset_score():
    fillcount = 0
    score1 = 0
    score2 = 0
    return fillcount, score1, score2

def draw_grid():
    for row in range(ROWS + 1):
        for col in range(COLS + 1):
            pygame.draw.circle(screen, 'black', (col * SIZE + 2 * PADDING, row * SIZE + 3 * PADDING), 4)

def reset_boxes():
    pos = None
    curr_box = None
    left = False
    right = False
    top = False
    bottom = False
    return pos, curr_box, left, right, top, bottom

def game_loop():
    gameover = False
    boxes = create_boxes()
    pos, curr_box, left, right, top, bottom = reset_boxes()
    fillcount, score1, score2 = reset_score()
    chance, players, player, next_chance = reset_players()

    running = True
    while running:
        screen.fill((220, 242, 238))
        draw_grid()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
            if event.type == pygame.MOUSEBUTTONUP:
                pos = None
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    return  # Return to levels menu
                if event.key == pygame.K_r:
                    gameover = False
                    boxes = create_boxes()
                    pos, curr_box, left, right, top, bottom = reset_boxes()
                    fillcount, score1, score2 = reset_score()
                    chance, players, player, next_chance = reset_players()

                if not gameover:
                    if event.key == pygame.K_UP:
                        top = True
                    if event.key == pygame.K_RIGHT:
                        right = True
                    if event.key == pygame.K_DOWN:
                        bottom = True
                    if event.key == pygame.K_LEFT:
                        left = True
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP:
                    top = False
                if event.key == pygame.K_RIGHT:
                    right = False
                if event.key == pygame.K_DOWN:
                    bottom = False
                if event.key == pygame.K_LEFT:
                    left = False

        for b in boxes:
            b.update(screen)
            if pos and b.box.collidepoint(pos):
                curr_box = b

        if curr_box:
            index = curr_box.index
            if not curr_box.winner:
                pygame.draw.circle(screen, 'red', (curr_box.box.centerx, curr_box.box.centery), 3)

            if top and not curr_box.sides[0]:
                curr_box.sides[0] = True
                if index - ROWS >= 0:
                    boxes[index - ROWS].sides[2] = True
                next_chance = True

            if right and not curr_box.sides[1]:
                curr_box.sides[1] = True
                if (index + 1) % COLS > 0:
                    boxes[index + 1].sides[3] = True
                next_chance = True

            if bottom and not curr_box.sides[2]:
                curr_box.sides[2] = True
                if index + ROWS < len(boxes):
                    boxes[index + ROWS].sides[0] = True
                next_chance = True

            if left and not curr_box.sides[3]:
                curr_box.sides[3] = True
                if (index % COLS) > 0:
                    boxes[index - 1].sides[1] = True
                next_chance = True

            result = curr_box.checkwin(player)
            if result:
                fillcount += result
                if player == 'X':
                    score1 += 1
                else:
                    score2 += 1
                if fillcount == ROWS * COLS:
                    print(f'Player 1: {score1}', f'Player 2: {score2}', sep='\t')
                    gameover = True
                next_chance = False

            if next_chance:
                chance = (chance + 1) % len(players)
                player = players[chance]
                next_chance = False

        p1 = font.render(f'Player 1 : {score1}', True, (6, 16, 89))
        p1rect = p1.get_rect()
        p1rect.x, p1rect.y = 2 * PADDING, 15

        p2 = font.render(f'Player 2 : {score2}', True, (6, 16, 89))
        p2rect = p2.get_rect()
        p2rect.right, p2rect.y = WIDTH - 3 * PADDING, 15

        screen.blit(p1, p1rect)
        screen.blit(p2, p2rect)

        if player == 'X':
            pygame.draw.line(screen, (6, 16, 89), (p1rect.x, p1rect.bottom + 2), (p1rect.right, p1rect.bottom + 2), 1)
        else:
            pygame.draw.line(screen, (6, 16, 89), (p2rect.x, p2rect.bottom + 2), (p2rect.right, p2rect.bottom + 2), 1)

        if gameover:
            rect = pygame.Rect((50, 100, WIDTH - 100, HEIGHT - 200))
            pygame.draw.rect(screen, 'darkslategray2', rect)
            pygame.draw.rect(screen, 'black', rect, 5)

            over = FONT.render('~ GAME OVER ~', True, 'black')
            screen.blit(over, (rect.centerx - over.get_width() / 2, rect.y + 125))

            if score1 > score2:
                winner = '1'
            elif score1 == score2:
                winner = '0'
            else:
                winner = '2'
            winner_img = font.render('Congratulations!', True, (99, 8, 135))
            screen.blit(winner_img, (rect.centerx - winner_img.get_width() / 2, rect.centery - 25)) 
            if winner == '1' or winner == '2':
                winner_img = font.render(f'Player {winner} Won', True, (99, 8, 135)) 
            else:
                winner_img = font.render("It's a draw", True, (99, 8, 135))
            screen.blit(winner_img, (rect.centerx - winner_img.get_width() / 2, rect.centery))

            message = 'r: restart     q: quit'
            message_img = font.render(message, True, 'black')
            screen.blit(message_img, (rect.centerx - message_img.get_width() / 2, rect.bottom - 75))

        pygame.display.update()

if __name__ == "__main__":
    game_loop()


