import numpy as np
import random

class DotsAndBoxes:
    def __init__(self, size):
        self.size = size
        self.dots = np.zeros((2 * size + 1, 2 * size + 1))
        self.reset()

    def reset(self):
        self.dots.fill(0)
        self.current_player = 1
        return self.get_state()

    def get_state(self):
        return tuple(self.dots.flatten())

    def is_valid_action(self, action):
        row, col = action
        return(0 <= row < 2 * self.size + 1) and (0 <= col < 2 * self.size + 1) and (self.dots[row, col] == 0)

    def step(self, action):
        row, col = action
        if not self.is_valid_action(action):
            return self.get_state(), -1, True

        self.dots[row, col] = 1
        reward = self.check_boxes(row, col)
        done = self.is_done()

        if reward > 0:
            reward = 1
        else:
            self.current_player = -self.current_player

        return self.get_state(), reward, done

    def is_done(self):
        return not np.any(self.dots == 0)

    def check_boxes(self, row, col):
        reward = 0
        if row % 2 == 0:
            if row > 0 and col > 0 and col < 2 * self.size and np.all(self.dots[row - 1: row, col - 1: col + 2] == 1):
                reward += 1
            if row < 2 * self.size and col > 0 and col < 2 * self.size and np.all(self.dots[row: row + 1, col - 1: col + 2] == 1):
                reward += 1
        else:
            if row > 0 and row < 2 * self.size and col > 0 and np.all(self.dots[row - 1: row + 2, col - 1: col] == 1):
                reward += 1
            if row > 0 and row < 2 * self.size and col < 2 * self.size and np.all(self.dots[row - 1: row + 2, col: col + 1] == 1):
                reward += 1
        return reward

class QLearningAgent:
    def __init__(self, size, alpha = 0.1, gamma = 0.9, epsilon = 0.1):
        self.size = size
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon
        self.q_table = {}
        self.actions = [(row, col) for row in range(2 * size + 1) for col in range(2 * size + 1) if (row % 2) ^ (col % 2 )]

    def get_q_value(self, state, action):
        return self.q_table.get((state, action), 0)

    def choose_action(self, state):
        if random.uniform(0, 1) < self.epsilon:
            return random.choice(self.actions)

        else:
            q_values = [self.get_q_value(state, action) for action in self.actions]
            max_q = max(q_values)
            max_q_actions = [self.actions[i] for i, q in enumerate(q_values) if q == max_q]
            return random.choice(max_q_actions)

    def update_q_table(self, state, action, reward, next_state):
        old_q = self.get_q_value(state, action)
        future_rewards = [self.get_q_value(next_state, action) for action in self.actions]
        max_future_q = max(future_rewards)
        new_q = old_q + self.alpha * (reward + self.gamma * max_future_q - old_q)
        self.q_table[(state, action)] = new_q

def train_agent(size, episodes):
    game_env = DotsAndBoxes(size)
    agent = QLearningAgent(size)
    for _ in range(episodes):
        state = game_env.reset()
        game_over = False
        while not game_over:
            action = agent.choose_action(state)
            next_state, reward, game_over = game_env.step(action)
            agent.update_q_table(state, action, reward, next_state)
            state = next_state
    return agent

agent = train_agent(2, 1)
for state_action, q_value in agent.q_table.items():
    print(f"State:{state_action[0]}, Action:{state_action[1]}, Q-value:{q_value}")



