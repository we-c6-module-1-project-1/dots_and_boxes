import pygame
import sys
import importlib

pygame.init()
pygame.font.init()

WIDTH = HEIGHT = 600
BUTTON_WIDTH = 200
BUTTON_HEIGHT = 60
BUTTON_COLOR = (0, 128, 255)
HOVER_COLOR = (0, 102, 204)
TEXT_COLOR = (255, 255, 255)
screen = pygame.display.set_mode((WIDTH, HEIGHT))

font = pygame.font.SysFont('cursive', 55)
button_font = pygame.font.SysFont('cursive', 32)

pygame.display.set_caption("Levels Menu")

def draw_button(text, x, y, width, height):
    mouse_pos = pygame.mouse.get_pos()
    button_rect = pygame.Rect(x, y, width, height)
    if button_rect.collidepoint(mouse_pos):
        pygame.draw.rect(screen, HOVER_COLOR, button_rect)
    else:
        pygame.draw.rect(screen, BUTTON_COLOR, button_rect)
    
    text_surface = button_font.render(text, True, TEXT_COLOR)
    text_rect = text_surface.get_rect(center=button_rect.center)
    screen.blit(text_surface, text_rect)

def draw_levels():
    screen.fill((220, 242, 238))
    
    title = font.render("Select Difficulty", True, 'black')
    screen.blit(title, (WIDTH // 2 - title.get_width() // 2, 50))
    
    draw_button("Easy", WIDTH // 2 - BUTTON_WIDTH // 2, 150, BUTTON_WIDTH, BUTTON_HEIGHT)
    draw_button("Medium", WIDTH // 2 - BUTTON_WIDTH // 2, 250, BUTTON_WIDTH, BUTTON_HEIGHT)
    draw_button("Hard", WIDTH // 2 - BUTTON_WIDTH // 2, 350, BUTTON_WIDTH, BUTTON_HEIGHT)

    instructions = button_font.render("Press ESC to quit", True, 'black')
    screen.blit(instructions, (WIDTH // 2 - instructions.get_width() // 2, HEIGHT - 100))

def levels_loop():
    pygame.display.set_mode((WIDTH, HEIGHT))
    running = True
    while running:
        draw_levels()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                if WIDTH // 2 - BUTTON_WIDTH // 2 <= mouse_pos[0] <= WIDTH // 2 + BUTTON_WIDTH // 2:
                    if 150 <= mouse_pos[1] <= 150 + BUTTON_HEIGHT:
                        import dots_and_boxes
                        dots_and_boxes.game_loop()  
                    elif 250 <= mouse_pos[1] <= 250 + BUTTON_HEIGHT:
                        import dotsAndBoxes_Medium
                        dotsAndBoxes_Medium.game_loop()
                    elif 350 <= mouse_pos[1] <= 350 + BUTTON_HEIGHT:
                        import dotsAndBoxes_Hard
                        importlib.reload(dotsAndBoxes_Hard)
                        
        pygame.display.update()

if __name__ == "__main__":
    levels_loop()


